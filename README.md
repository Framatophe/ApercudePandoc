# Un aperçu de Pandoc

Par : **Massimiliano Dominici**

## Résumé

Cet article est un court aperçu de Pandoc, un utilitaire dédié à  la conversion de documents formatés en Markdown vers de multiples formats de sortie, y compris LaTeX et HTML.

---

Cet article est paru sous le titre «&nbsp;[An overview of Pandoc](https://tug.org/TUGboat/tb35-1/tb109dominici.pdf)&nbsp;», _TUGboat_ 35:1, 2014, pp.&nbsp; 44-50. Originellement publié sous «&nbsp;[Una panoramica su Pandoc](http://www.guitex.org/home/images/ArsTeXnica/AT015/pandoc.pdf)&nbsp;», _ArsTEXnica_ 15, avril 2013, pp.&nbsp;31-38. Traduction de l''anglais par [Christophe Masutti](http://christophe.masutti.name), avec l''aimable autorisation de l''auteur. Licence du document&nbsp;: [CC By-Sa](http://creativecommons.org/licenses/by-sa/4.0/).



## Comment compiler ce document ?



Pour une sortie LaTeX, avec le template LaTeX (ne pas oublier d'installer `pandoc-citeproc`) :

      $pandoc --listings --bibliography=biblio.bib  --biblatex metadata.yaml --number-sections -s --template=modele.latex --toc source.markdown -o sortie.tex


Pour la sortie PDF :

      $pdflatex sortie.tex
      $biber sortie
      $pdflatex sortie.tex


Pour la sortie HTML :

      $pandoc -s -S --toc --template=modele.html  -H style.css --number-sections --bibliography=biblio.bib  metadata.yaml source.markdown -o sortie.html


